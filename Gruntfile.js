module.exports = function (grunt){

  require('time-grunt')(grunt);

  let conf = {
    cwd: 'src/',
    dest: 'dist/',

    cssCwd: 'src/scss/',
    cssDest: 'dist/css/',

    npm: 'node_modules',
    jsCwd: 'src/scripts/',
    jsDest: 'dist/js/',
    jsCompile: 'src/scripts/_tmp/',

    imgCwd: 'src/images',
    imgDest: 'dist/images',

    fontCwd: 'src/fonts',
    fontDest: 'dist/fonts',

    tmplCwd: 'src/templates/',
    tmplDest: 'dist/templates/',

    viewsCwd: 'src/views/',
    viewsDest: 'dist/',

    incCwd: 'src/inc/',
    incDest: 'dist/inc/',

    sassdocDest: 'docs/sassdoc',
    tsdocDest: 'docs/typedoc',

    env: grunt.file.readYAML('environment.yml')
  };

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    /**
     * Replace variables in files depending on environment
     */
    replace: {
      views: {
        options: {
          patterns: [
            {
              match: /(@@\w+@@)/g,
              replacement: function(match) {
                return conf.env[match.replace(/@@/g, '')];
              }
            }
          ]
        },
        files: [
          {
            expand: true,
            cwd: conf.viewsCwd,
            src: ['**'],
            dest: conf.viewsDest
          }
        ]
      },
      templates: {
        options: {
          patterns: [
            {
              match: /(@@\w+@@)/g,
              replacement: function(match) {
                return conf.env[match.replace(/@@/g, '')];
              }
            }
          ]
        },
        files: [
          {
            expand: true,
            cwd: conf.tmplCwd,
            src: ['**'],
            dest: conf.tmplDest
          }
        ]
      },
    },


    /**
     * Compile TypeScript
     * https://www.npmjs.com/package/grunt-ts
     */
    ts: {
      options: {
        allowJs: false,
        target: 'es5',
        rootDir: conf.jsCwd,
        sourceMap: false,
        strictNullChecks: false,
        noImplicitReturns: false,
        noImplicitThis: false,
        noUnusedLocals: false,
        pretty: true,
        experimentalDecorators: true,
        emitDecoratorMetadata: true
      },

      default: {
        src: [
          conf.jsCwd + '**/*.ts'
        ],
        outDir: conf.jsCompile,
        reference: conf.jsCwd + 'typings/references.ts'
      }
    },


    /**
     * Typedoc
     * http://typedoc.org/api/
     */
    typedoc: {
      build: {
        options: {
          module: 'commonjs',
          target: 'es5',
          out: conf.tsdocDest,
          name: 'My project title',
          exclude: [
            conf.jsCwd + 'pubsub/**/*'
          ],
          ignoreCompilerErrors: true
        },
        src: conf.jsCwd + 'components/**/*.ts'
      }
    },

    /**
     * Minify files with UglifyJS and move to dist
     * https://github.com/gruntjs/grunt-contrib-uglify
     */
    uglify: {
      libraries: {
        files: [{
          src: [
            conf.npm + "/jQuery/tmp/jquery.js",
            conf.npm + "/jquery.cookie/jquery.cookie.js",
            conf.npm + "/nunjucks/browser/nunjucks.js",
            conf.npm + "/lodash/lodash.js",
            conf.npm + "/q/q.js",
            conf.npm + "/document-register-element/build/document-register-element.js",
            conf.npm + "/chart.js/dist/Chart.js",
          ],
          dest: conf.jsDest + "libraries.min.js"
        }]
      },

      vt: {
        files: [
          {
            src: [
              conf.jsCompile + '**/*.js',
              !conf.jsCompile + 'typings/**/*.js'
            ],
            dest: conf.jsDest + 'vt.min.js'
          }
        ]
      }
    },


    /**
     * Compile Sass
     */
    sass: {
      dist: {
        options: {
          sourceMap: true,
          outputStyle: 'compressed',
          includePaths: [conf.cssCwd]
        },

        files: [{
          expand: true,
          cwd: conf.cssCwd,
          src: [
            '*.scss'
          ],
          dest: conf.cssDest,
          ext: '.min.css'
        }]
      }
    },


    postcss: {
      options: {
        map: {
          inline: false,
          annotation: conf.cssDest
        },
        processors: [
          require('autoprefixer')({browsers: 'last 2 versions, safari 8'}), // add vendor prefixes
          require('postcss-discard-comments')({removeAll: true}) // remove comments
        ]
      },
      dist: {
        src: conf.cssDest + '*.css'
      }
    },


    /**
     * SassDoc
     * http://sassdoc.com/
     */
    sassdoc: {
      default: {
        src: conf.cssCwd + '**/*.scss',
        options: {
          dest: conf.sassdocDest
        }
      }
    },


    /**
     * Copy Files & Dependencies
     */
    copy: {
      views: {
        files: [
          {
            expand: true,
            cwd: conf.viewsCwd,
            src: ['**/*.html'],
            dest: conf.viewsDest
          }
        ]
      },
      images: {
        files: [
          {
            expand: true,
            flatten: false,
            cwd: conf.imgCwd,
            src: '**',
            dest: conf.imgDest
          }
        ]
      },
      fonts: {
        files: [
          {
            expand: true,
            flatten: false,
            cwd: conf.fontCwd,
            src: ['*/**.otf', '*/*.eot', '*/**.svg', '*/**.ttf', '*/**.woff', '*/**.woff2'],
            dest: conf.fontDest
          }
        ]
      },
      templates: {
        files: [
          {
            expand: true,
            flatten: false,
            cwd: conf.tmplCwd,
            src: ['**/*.html'],
            dest: conf.tmplDest
          }
        ]
      },
      inc: {
        files: [
          {
            expand: true,
            flatten: false,
            cwd: conf.incCwd,
            src: ['**.*'],
            dest: conf.incDest
          }
        ]
      }
    },


    /**
     * Watch Tasks
     */
    watch: {
      css: {
        files: [
          conf.cssCwd + '**/*.scss'
        ],
        tasks: ['sass', 'sassdoc'],
        options: {
          spawn: false
        }
      },
      scripts: {
        files: [
          conf.jsCwd + '**/*.ts'
        ],
        tasks: ['ts', 'uglify:vt'],
        options: {
          spawn: false
        }
      },
      views: {
        files: [
          conf.viewsCwd + '**/*.html'
        ],
        tasks: ['copy:views', 'replace'],
        options: {
          spawn: false
        }
      },
      templates: {
        files: [
          conf.tmplCwd + "**/*.html"
        ],
        tasks: ['copy:templates', 'replace'],
        options: {
          spawn: false
        }
      }
    }
  });


  grunt.loadNpmTasks("grunt-ts");
  grunt.loadNpmTasks("grunt-contrib-uglify");
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-postcss');
  grunt.loadNpmTasks('grunt-replace');

  grunt.loadNpmTasks('grunt-sassdoc');
  grunt.loadNpmTasks('grunt-typedoc');

  grunt.registerTask("set_dev_options", "Set Config variables for dev tasks", function (){
    grunt.config.set("uglify.options.sourceMap", true);
    grunt.config.set("uglify.options.mangle", false);
    grunt.config.set("uglify.options.beautify", true);
  });

  // Define Task(s)
  grunt.registerTask('default', ['ts', 'uglify', 'sass', 'postcss', 'copy', 'replace']);
  grunt.registerTask('docs', ['set_dev_options', 'default', 'sassdoc', 'typedoc']);
  grunt.registerTask('dev', ['docs', 'watch']);
};
