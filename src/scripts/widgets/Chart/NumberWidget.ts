///<reference path='../../typings/references.ts' />

class ChartNumberWidget extends BaseWidget {

    public init(): Q.Promise<boolean> {
        const deferred = Q.defer<boolean>();

        this.handleContent();

        deferred.resolve(true);
        return deferred.promise;
    }


    /**
     * render template and send Message in PubSub
     */
    public handleContent() {
        let tplData = this.options;

        this.getTemplate("view.html", tplData).then((t: JQuery) => {
            this.config.container.html(t);
        });
    }
}