///<reference path='../../typings/references.ts' />

class ChartBarWidget extends BaseWidget {

    public init(): Q.Promise<boolean> {
        const deferred = Q.defer<boolean>();

        this.handleContent();

        deferred.resolve(true);
        return deferred.promise;
    }


    /**
     * render template and send Message in PubSub
     */
    public handleContent() {
        let tplData: any = {
            uuid: this.config.uuid,
            chart: this.options
        };

        this.getTemplate("view.html", tplData).then((t: JQuery) => {
            this.config.container.html(t);

            this.renderChart({
                labels: this.options.label,
                datasets: [{
                    label: this.options.title,
                    backgroundColor: 'rgb(255, 0, 0)',
                    borderWidth: 0,
                    data: this.options.value,
                }]
            })
        });
    }


    /**
     *
     * @param dataset
     */
    private renderChart(dataset: any) {
        const ctx = document.getElementById(this.config.uuid);

        let chart = new Chart(ctx, {
            type: 'bar',
            data: dataset,
            options: {
                legend: {
                    display: false
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            offsetGridLines: true
                        }
                    }],
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    }
}