///<reference path='../../typings/references.ts' />

class OverlayWaitWidget extends BaseWidget {

    private overlayChannel: PubSub;

    public init(): Q.Promise<boolean> {
        const deferred = Q.defer<boolean>();

        this.overlayChannel = PubSub.join('#Overlay');
        const tplData = {};

        this.getTemplate("view.html", tplData).then((t: JQuery) => {
            this.config.container.html(t);
            this.registerPubSub();
        });

        deferred.resolve(true);
        return deferred.promise;
    }


    /**
     * register PubSub
     * > open Overlay
     * > close Overlay
     */
    public registerPubSub() {
        this.overlayChannel.on('wait-open', () => {
            this.config.container.children().addClass('-in');
        });

        this.overlayChannel.on('wait-close', () => {
            this.config.container.children().removeClass('-in');
        });
    }
}