///<reference path='../../typings/references.ts' />

class NavigationBreadcrumbWidget extends BaseWidget {

    protected insideFactoryChannel: PubSub;
    protected $breadcrumb: JQuery;

    protected currentData: any = {};

    public init(): Q.Promise<boolean> {
        const deferred = Q.defer<boolean>();

        this.insideFactoryChannel = PubSub.join('#Inside');

        this.handleContent();

        this.insideFactoryChannel.on('show-data', (resp) => {
            if (resp.year) {
                this.currentData = {};
                this.currentData['year'] = resp.year;

                this.appendBreadcrumb();
            }

            if (resp.season) {
                this.currentData['season'] = resp.season.label;
                delete this.currentData.gameday;

                this.appendBreadcrumb();
            }

            if (resp.gameday) {
                this.currentData['gameday'] = resp.gameday.number + '. Spieltag';

                this.appendBreadcrumb();
            }
        });

        deferred.resolve(true);
        return deferred.promise;
    }


    /**
     * render template and send Message in PubSub
     */
    public handleContent() {
        let data = {};

        this.getTemplate("view.html", data).then((t: JQuery) => {
            this.config.container.html(t);

            this.$breadcrumb = this.config.container.find('ul');
        });
    }


    /**
     *
     */
    private appendBreadcrumb() {
        this.$breadcrumb.html('');
        _.each(this.currentData, (item) => {
            let $li: any = jQuery('<li>' + item + '</li>');
            this.$breadcrumb.append($li);
        });
    }
}