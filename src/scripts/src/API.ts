///<reference path='../typings/references.ts' />

class API {

    protected errorChannel: PubSub;

    /**
     *
     * @param method
     * @param uri
     * @param data
     * @param requestOptions
     * @returns {Promise<*>}
     */
    private makeRequest(method: ApiRequestType, uri: string, data: any, requestOptions?: any): Q.Promise<any> {
        const deferred = Q.defer<any>();
        this.errorChannel = PubSub.join('#Error');

        let url: string = '';
        if (uri.startsWith('https://') || uri.startsWith('http://')) {
            url = uri;
        } else {
            url = app_settings.api_url + uri;
        }

        const options = {
            'url': url,
            'type': method.toString(),
            'dataType': 'json',
            'beforeSend': (request) => {

            },
            'success': (data: any, textStatus, request) => {
                return deferred.resolve(data);
            },
            'error': (xhr: JQueryXHR, textStatus: string, errorThrown: string) => {
                const rejectionContext: any = {
                    "xhr": xhr,
                    "textStatus": textStatus,
                    "errorThrown": errorThrown,
                    "responseJson": xhr.responseJSON
                };

                this.errorChannel.send('xhr', xhr);

                return deferred.reject(rejectionContext);
            },
            'xhrFields': {
            }
        };

        if (data) {
            options['contentType'] = 'application/json; charset=utf-8';

            if (typeof data === 'string') {
                options['data'] = data;
            } else {
                options['data'] = JSON.stringify(data);
            }
        }

        $.extend(options, requestOptions);
        $.ajax(options.url, options);

        return deferred.promise;
    }

    /**
     *
     * @param uri
     * @param requestOptions
     * @returns {Q.Promise<any>}
     */
    public get(uri: string, requestOptions?: any): Q.Promise<any> {
        return this.makeRequest(ApiRequestType.GET, uri, null, requestOptions);
    }


    /**
     *
     * @param uri
     * @param data
     * @param requestOptions
     * @returns {Q.Promise<any>}
     */
    public post(uri: string, data: any, requestOptions?: any): Q.Promise<any> {
        return this.makeRequest(ApiRequestType.POST, uri, data, requestOptions);
    }


    /**
     *
     * @param uri
     * @param data
     * @param requestOptions
     * @returns {Q.Promise<any>}
     */
    public put(uri: string, data: any, requestOptions?: any): Q.Promise<any> {
        return this.makeRequest(ApiRequestType.PUT, uri, data, requestOptions);
    }


    /**
     *
     * @param uri
     * @param data
     * @param requestOptions
     * @returns {Q.Promise<any>}
     */
    public delete(uri: string, data: any, requestOptions?: any): Q.Promise<any> {
        return this.makeRequest(ApiRequestType.DELETE, uri, data, requestOptions);
    }


    /**
     *
     * @param uri
     * @param data
     * @param requestOptions
     * @returns {Q.Promise<any>}
     */
    public patch(uri: string, data?: any, requestOptions?: any): Q.Promise<any> {
        return this.makeRequest(ApiRequestType.PATCH, uri, data, requestOptions);
    }
}

interface AjaxResponse {
    context: any[],
    success: boolean,
    error: string
}