///<reference path='../typings/references.ts' />

class TemplateFilters {

    public registerFilters(env: any): void {

        /**
         * slice string to return {count} letters
         */
        env.addFilter('shorten', (str, count) => {
            return str.slice(0, count || 5);
        });


        env.addFilter('round', (int, count) => {
            let output: any = 0;

            if(Assert.isNumber(int)){
                output = Math.round(int * 100) / 100;
            }

            return output;
        });


        /**
         * capitalize First Letter
         */
        env.addFilter('capitalizeFirstLetter', (str) => {
            return str.charAt(0).toUpperCase() + str.slice(1);
        });


        /**
         * set container size
         */
        env.addFilter('widgetSize', (str) => {
            const arr: any = {
                'default': 'widget-12col',
                'number': 'widget-3col',
                'numberchart': 'widget-3col',
                'numberrelation': 'widget-3col',
                'numberpercentage': 'widget-3col',
                'ranking': 'widget-6col',
                'bar': 'widget-12col',
                'spread': 'widget-6col',
                'piechart': 'widget-6col',
                'datatable': 'widget-12col'
            };

            return arr[str] || arr['default'];
        });
    }
}