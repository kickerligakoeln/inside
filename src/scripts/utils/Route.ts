///<reference path='../typings/references.ts' />

class Route {

    /**
     * get Url Parameter
     *
     * @param name
     */
    public static getUrlParam(name) {
        let results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if (Assert.isUndefinedNullOrEmpty(results)) {
            return null;
        }
        else {
            return decodeURI(results[1]) || true;
        }

        // return new URLSearchParams(window.location.search); // do not work in IE11 ?!
    }


    /**
     * route to internal page
     *
     * @param path
     */
    public to(path) {
        return app_settings['base_url'] + path + '.html';
    }


    /**
     * route to api page
     *
     * @param path
     */
    public toApi(path) {
        return app_settings['api_url'] + path;
    }


}
