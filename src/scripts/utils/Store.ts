///<reference path='../typings/references.ts' />

class Store {

    protected prefix: string = '';

    /**
     * get item from storage
     *
     * @param name
     * @returns {{}}
     */
    public get(name: any) {
        let data = jQuery.cookie(this.prefix + name);

        try {
            return jQuery.parseJSON(data);
        } catch (e) {
            return data;
        }
    };


    /**
     * save item in storage
     * TODO: "expires" in options
     *
     * @param name
     * @param data
     * @param expireDateString
     */
    public set(name: string, data: any, expireDateString?: string) {
        const expireDate = new Date(expireDateString);

        let values: any = null;

        if (typeof data === 'string') {
            values = data;
        } else {
            values = JSON.stringify(data);
        }

        const options: any = {
            path: '/',
            expires: expireDate
        };

        jQuery.cookie(this.prefix + name, values, options);
    };


    /**
     * remove item from storage
     *
     * @param name
     */
    public remove(name: string) {
        jQuery.cookie(this.prefix + name, null, {path: '/'});
    }
}