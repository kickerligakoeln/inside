///<reference path='../typings/references.ts' />

class ResultsTotalComponent extends BaseComponent {

    public init(): Q.Promise<boolean> {
        const deferred = Q.defer<boolean>();

        this.api.get('/getResultsTotal.php', {}).then((response) => {
            console.info('getResultsTotal', response);

            this.getTemplate("view.html", {}).then((t: JQuery) => {
                this.config.container.html(t);
                this.bind();

                deferred.resolve(true);
            });
        });

        return deferred.promise;
    }
}