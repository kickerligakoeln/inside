///<reference path='../typings/references.ts' />

class BaseComponent {

    protected config: any;
    protected api: API = new API();

    constructor(config) {
        this.config = config;
    }

    public init(): Q.Promise<boolean> {
        throw new Error('init() must be implemented!');
    }


    /**
     * get rendered Template
     *
     * @param templateName
     * @param data
     */
    public getTemplate(templateName: string, data?: any): Q.Promise<any> {
        const deferred = Q.defer<any>();
        const templateUrl = app_settings.base_url + '/templates/components/' + this.config.templatePath;

        BaseComponent.render(templateUrl, templateName, data).then((template) => {
            deferred.resolve(template);
        });

        return deferred.promise;
    }


    /**
     * render Template with Nunjucks
     * https://mozilla.github.io/nunjucks/templating.html
     *
     * @param templateUrl
     * @param templateName
     * @param context
     */
    private static render(templateUrl, templateName, context): Q.Promise<JQuery> {
        const deferred = Q.defer<JQuery>();

        let env = nunjucks.configure(templateUrl, {autoescape: true});
        new TemplateFilters().registerFilters(env);

        let template = nunjucks.render(templateName, context);

        deferred.resolve(template);
        return deferred.promise;
    }


    /**
     * register nested vt-elements and start
     * TODO: ask andy for best practice?!
     */
    public bind() {
        const vt = new VT3000();

        vt.fetchElementsFromDom(this.config.container);
    }
}