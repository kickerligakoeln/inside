///<reference path='../typings/references.ts' />

class TutorialComponent extends BaseComponent {

    public init(): Q.Promise<boolean> {
        const deferred = Q.defer<boolean>();

        this.handleContent().then(() => {
            deferred.resolve(true);
        });

        return deferred.promise;
    }


    /**
     * fetch data from endpoint and render template
     */
    public handleContent(): Q.Promise<boolean> {
        const deferred = Q.defer<boolean>();

        this.api.get('https://jsonplaceholder.typicode.com/users').then((response) => {
            let templateData: any = {
                data: response.slice(0, 4)
            };

            this.getTemplate("view.html", templateData).then((t: JQuery) => {
                this.config.container.append(t);
                this.bind();

                deferred.resolve(true);
            });
        });

        return deferred.promise;
    }


}