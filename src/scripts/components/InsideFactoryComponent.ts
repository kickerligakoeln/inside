///<reference path='../typings/references.ts' />

class InsideFactoryComponent extends BaseComponent {

    protected insideFactoryChannel: PubSub;
    protected overlayChannel: PubSub;

    public init(): Q.Promise<boolean> {
        const deferred = Q.defer<boolean>();

        this.insideFactoryChannel = PubSub.join('#Inside');
        this.overlayChannel = PubSub.join('#Overlay');

        this.insideFactoryChannel.on('show-data', (channelResponse) => {
            this.overlayChannel.send('wait-open', true);

            if (channelResponse.season) {
                this.api.get('/getSeasonsDashboard.php?season=' + channelResponse.season.id).then((response) => {
                    // console.log('getSeasonDashboard', response);
                    this.appendWidgets(response);
                });
            }

            if (channelResponse.gameday) {
                this.api.get('/getGamedayDashboard.php?gameday=' + channelResponse.gameday.id).then((response) => {
                    // console.log('getGamedayDashboard', response);
                    this.appendWidgets(response);
                });
            }
        });

        deferred.resolve(true);
        return deferred.promise;
    }


    /**
     *
     * @param allWidgets
     */
    public appendWidgets(allWidgets) {
        this.getTemplate("view.html", {allWidgets}).then((t: JQuery) => {
            this.config.container.html(t);
            let $widgetFactory = this.config.container.find('#widget-factory');

            _.each(allWidgets, (widget) => {
                if (widget.type) {
                    let $elem = jQuery('<vt-widget />');
                    $elem.attr('name', 'Chart/' + widget.type);
                    $elem.addClass(widget.css);
                    $elem.data(widget);

                    $widgetFactory.append($elem);
                }
            });

            this.bind();
            this.overlayChannel.send('wait-close', true);
        });
    }
}
