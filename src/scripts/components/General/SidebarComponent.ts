///<reference path='../../typings/references.ts' />

class GeneralSidebarComponent extends BaseComponent {

    protected insideFactoryChannel: PubSub;
    protected overlayChannel: PubSub;

    protected years = [2019, 2018, 2017, 2016, 2015, 2014, 2013, 2012, 2011, 2010];
    protected $sidebarSeasons: JQuery;

    public init(): Q.Promise<boolean> {
        const deferred = Q.defer<boolean>();

        this.insideFactoryChannel = PubSub.join('#Inside');
        this.overlayChannel = PubSub.join('#Overlay');

        this.appendYears();
        this.registerClickHandler();

        deferred.resolve(true);
        return deferred.promise;
    }


    /**
     * slide sidebar left / right by class
     */
    public toggleSidebar() {
        this.config.container.children().toggleClass('-in');
    }


    /**
     *
     */
    private appendYears() {
        const tmplData: any = {
            years: this.years
        };

        this.getTemplate("view.html", tmplData).then((t: JQuery) => {
            this.config.container.html(t);
            this.bind();

            this.$sidebarSeasons = this.config.container.find('#navbar-seasons');
        });
    }


    /**
     *
     * @param currentYear
     */
    public appendSeasons(currentYear) {
        this.api.get('/getSeasons.php?year=' + currentYear).then((response) => {
            let tmplData: any = {
                year: currentYear,
                seasons: response
            };

            this.getTemplate("navbar-seasons.html", tmplData).then((t) => {
                this.$sidebarSeasons.html(t);
                this.bind();
                this.overlayChannel.send('wait-close', true);
            });
        });
    }


    /**
     * register Event Handler
     * > click: getLeague
     * > click: showGameday
     */
    private registerClickHandler() {
        this.config.container.on('click', '[data-toggle="sidebar"]', (e) => {
            this.toggleSidebar();
        });

        this.config.container.on('click', '[data-click="showYear"]', (e) => {
            e.preventDefault();
            const $elem = jQuery(e.currentTarget);
            const currentYear = $elem.attr('data-id');

            this.config.container.find('[data-click="showYear"]').removeClass('-active');
            $elem.addClass('-active');

            this.appendSeasons(currentYear);
            this.insideFactoryChannel.send('show-data', {
                year: currentYear
            });
        });

        this.config.container.on('click', '[data-click="showSeason"]', (e) => {
            e.preventDefault();
            const $elem = jQuery(e.currentTarget);
            const seasonId = $elem.attr('data-id');
            const seasonLabel = $elem.text();

            this.config.container.find('[data-click="showSeason"]').removeClass('-active');
            $elem.addClass('-active');

            this.toggleSidebar();
            this.insideFactoryChannel.send('show-data', {
                season: {
                    id: seasonId,
                    label: seasonLabel
                }
            });
        });
    }
}