///<reference path='../../typings/references.ts' />

class FilterGamedaysComponent extends BaseComponent {

    protected insideFactoryChannel: PubSub;

    public init(): Q.Promise<boolean> {
        const deferred = Q.defer<boolean>();

        this.insideFactoryChannel = PubSub.join('#Inside');
        this.insideFactoryChannel.on('show-data', (response) => {
            if (response.season) {
                this.getGamedays(response.season.id);
            }

            if (response.year) {
                this.showSelection(false);
            }
        });

        this.getTemplate("view.html", {}).then((t: JQuery) => {
            this.config.container.html(t);
            this.bind();

            this.registerEventHandler();
        });

        deferred.resolve(true);
        return deferred.promise;
    }


    /**
     *
     * @param showSelection
     */
    private showSelection(showSelection: boolean) {
        if (showSelection) {
            this.config.container.children().addClass('-in');
        } else {
            this.config.container.children().removeClass('-in');
        }
    }


    /**
     *
     * @param seasonId
     */
    private getGamedays(seasonId) {
        this.api.get("/getGamedays.php?season=" + seasonId).then((response) => {

            let $select = this.config.container.find('select');
            $select.html('');

            if (response.length > 0) {
                this.showSelection(true);

                _.each(response, (gameday) => {
                    let option = jQuery('<option>' + gameday.number + '. Spieltag</option>');
                    option.attr('value', gameday.number);
                    option.data(gameday);

                    this.config.container.find('select').append(option);
                });
            }
        });
    }


    /**
     * register Event Handler
     * > change: select gameday
     */
    private registerEventHandler() {
        this.config.container.on('change', 'select', (e) => {
            const $elem = jQuery(e.currentTarget);
            const elemData = $elem.find('option[value="' + $elem.val() + '"]').data();

            this.insideFactoryChannel.send('show-data', {
                gameday: elemData
            });
        });
    }
}