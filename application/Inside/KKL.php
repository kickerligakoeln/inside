<?php

namespace KKL;

require_once(__DIR__ . '/../config/config.php');

class KKL {
  
  /**
   * @param $gameday
   * @return array|mixed
   */
  public function getMatchesPerGameday($gameday){
    $jsonMatches = file_get_contents(KKL_API_URL . "/gamedays/" . $gameday . "/matches");
  
    if ($jsonMatches){
      $allMatches = json_decode($jsonMatches, true);
    
      return $allMatches;
    } else{
      return array();
    }
  }
  
  
  /**
   * @param $season
   * @return array|mixed
   */
  public function getMatchesPerSeason($season) {
    $jsonMatches = file_get_contents(KKL_API_URL . "/gamedays?seasonId=" . $season . "&embed=matches");
  
    if ($jsonMatches){
      $allMatches = array();
      $allGamedays = json_decode($jsonMatches, true);
  
      foreach ($allGamedays as $gameday){
        foreach ($gameday["_embedded"]["matches"] as $match)
        $allMatches[] = $match;
      }
    
      return $allMatches;
    } else{
      return array();
    }
  }
  
  
  /**
   * @param $gameday
   * @return array|mixed
   */
  public function getScoresheetsPerGameday($gameday){
    $jsonMatches = file_get_contents(KKL_SPIELBOGEN_URL . "/getAllScoresheetsByMatchInfo?gamedayId=" . $gameday);
    
    if ($jsonMatches){
      $allMatches = json_decode($jsonMatches, true);
      
      return $allMatches;
    } else{
      return array();
    }
  }
  
  
  /**
   * @param $season
   * @return array|mixed
   */
  public function getScoresheetsPerSeason($season){
    $jsonMatches = file_get_contents(KKL_SPIELBOGEN_URL . "/getAllScoresheetsByMatchInfo?currentSeason=" . $season);
    
    if ($jsonMatches){
      $allMatches = json_decode($jsonMatches, true);
      
      return $allMatches;
    } else{
      return array();
    }
  }
}