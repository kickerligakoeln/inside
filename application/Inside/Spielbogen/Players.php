<?php

namespace Inside\Spielbogen;

require_once(__DIR__ . '/MatchUtils.php');

use Inside\Spielbogen\MatchUtils;

class Players{
  
  private $matchUtils;
  
  /**
   * BarChart constructor.
   */
  public function __construct() {
    $this->matchUtils = new MatchUtils();
  }
  
  
  /**
   * @param $scoresheets
   * @return array
   */
  public function total($scoresheets) {
    $output = array(
      "type" => "Number",
      "css" => "widget-3col",
      "title" => "Spieler Gesamt",
      "value" => array(
        "total" => 0,
        "ratio" => array(0, 0)
      )
    );
  
    foreach ($scoresheets as $match){
      $playerHome = count($match["team_home"]["player"]);
      $playerAway = count($match["team_guest"]["player"]);
      
      $output["value"]["total"] += ($playerHome + $playerAway);
      $output["value"]["ratio"][0] += $playerHome;
      $output["value"]["ratio"][1] += $playerAway;
    }
  
    return $output;
  }
}