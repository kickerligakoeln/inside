<?php

namespace Inside\Ligatool;

require_once(__DIR__ . '/MatchUtils.php');

use Inside\Ligatool\MatchUtils;

class Fixture{
  
  private $matchUtils;
  
  /**
   * BarChart constructor.
   */
  public function __construct() {
    $this->matchUtils = new MatchUtils();
  }
  
  
  /**
   * @param $matches
   * @return array
   */
  public function perWeekday($matches) {
    $flag = true;
    $output = array(
      "type" => "Bar",
      "css" => "widget-6col",
      "title" => "Begegnungen pro Wochentag",
      "label" => array('Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa', 'So'),
      "value" => array(0, 0, 0, 0, 0, 0, 0)
    );
  
    foreach ($matches as $match){
      if ($this->matchUtils->hasFixture($match)){
        $fixture = $match["fixture"];
        
        $output["value"][(date("N", strtotime($fixture))) - 1]++;
        $flag = false;
      }
    }
  
    if ($flag){
      $output = array();
    }
  
    return $output;
  }
}