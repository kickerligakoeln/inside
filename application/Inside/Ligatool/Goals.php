<?php

namespace Inside\Ligatool;

require_once(__DIR__ . '/MatchUtils.php');

use Inside\Ligatool\MatchUtils;

class Goals{
  
  private $matchUtils;
  
  /**
   * Number constructor.
   */
  public function __construct() {
    $this->matchUtils = new MatchUtils();
  }
  
  
  /**
   *
   * @param $matches
   * @return array
   */
  public function total($matches){
    $output = array(
      "type" => "Number",
      "css" => "widget-3col",
      "title" => "Tore Gesamt",
      "value" => array(
        "total" => 0,
        "ratio" => array(0, 0)
      )
    );
    
    return $output;
  }
}