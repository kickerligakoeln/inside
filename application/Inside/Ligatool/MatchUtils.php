<?php

namespace Inside\Ligatool;

class MatchUtils{
  
  /**
   * @param $match
   * @return bool
   */
  public function hasScore($match){
    return isset($match["scoreHome"]) && isset($match["scoreAway"]) && !($match["scoreHome"] == 0 && $match["scoreAway"] == 0);
  }
  
  
  /**
   * @param $match
   * @return bool
   */
  public function hasFixture($match){
    return isset($match["fixture"]) && $match["fixture"] != NULL && $match["fixture"] != "0000-00-00 00:00:00";
  }
  
  
  /**
   * @param $matches
   * @return array
   */
  public function getFilteredMatches($matches){
    $output = array();
    
    foreach ($matches as $match) {
      if($this->hasScore($match)) {
        array_push($output, $match);
      }
    }
    
    return $output;
  }
}