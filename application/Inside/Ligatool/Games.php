<?php

namespace Inside\Ligatool;

require_once(__DIR__ . '/MatchUtils.php');

use Inside\Ligatool\MatchUtils;

class Games{
  
  private $matchUtils;
  
  /**
   * BarChart constructor.
   */
  public function __construct() {
    $this->matchUtils = new MatchUtils();
  }
  
  
  /**
   * @param $matches
   * @return array
   */
  public function total($matches) {
    $output = array(
      "type" => "Number",
      "css" => "widget-3col",
      "title" => "Spiele Gesamt",
      "value" => array(
        "total" => 0
      )
    );
    
    foreach ($matches as $match) {
      if($this->matchUtils->hasScore($match)) {
        $output["value"]["total"]++;
      }
    }
    
    return $output;
  }
  
  
  /**
   * @param $matches
   * @return array
   */
  public function results($matches){
    $output = array(
      "type" => "Bar",
      "css" => "widget-5col",
      "title" => "Ergebnisse",
      "label" => array('10:10', '11:9', '12:8', '13:7', '14:6', '15:5', '16:4', '17:3', '18:2', '19:1', '20:0'),
      "value" => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
    );
  
    foreach ($matches as $match){
      if ($this->matchUtils->hasScore($match)){
        $scoreHome = $match["scoreHome"];
        $scoreGuest = $match["scoreAway"];
      
        if ($scoreHome >= 10){
          $score = $scoreHome;
        } else{
          $score = $scoreGuest;
        }
      
        $output["value"][$score - 10] = $output["value"][$score - 10] + 1;
      }
    }
    
    return $output;
  }
  
  
  /**
   * @param $matches
   * @return array
   */
  public function distribution($matches){
    $output = array(
      "type" => "Bar",
      "css" => "widget-5col",
      "title" => "Verteilung Ergebnisse",
      "label" => array(),
      "value" => array()
    );
  
    foreach ($matches as $key => $match){
      if ($this->matchUtils->hasScore($match)){
        $scoreHome = $match["scoreHome"];
        $scoreGuest = $match["scoreAway"];
      
        $output["label"][] = '';
        $output["value"][] = $scoreHome - $scoreGuest;
      }
    }
    
    return $output;
  }
  
  
  /**
   * @param $matches
   * @return array
   */
  public function ratio($matches){
    $output = array(
      "type" => "Bar",
      "css" => "widget-2col",
      "title" => "Anzahl Siege",
      "label" => array('Heim', 'Gast'),
      "value" => array(0, 0)
    );
  
    foreach ($matches as $key => $match){
      if ($this->matchUtils->hasScore($match)){
        $scoreHome = $match["scoreHome"];
        $scoreGuest = $match["scoreAway"];
      
        if ($scoreHome > 10){
          $output["value"][0]++;
        }
      
        if ($scoreGuest > 10){
          $output["value"][1]++;
        }
      }
    }
    
    return $output;
  }
  
  
  /**
   * @param $matches
   * @return array
   */
  public function failures($matches){
    $output = array(
      "type" => "Number",
      "css" => "widget-3col",
      "title" => "Spielausfälle",
      "value" => array(
        "total" => 0,
        "ratio" => array(0, 0)
      )
    );
    
    foreach ($matches as $match) {
      $scoreHome = $match["scoreHome"];
      $scoreGuest = $match["scoreAway"];
      
      if ($scoreHome === 20){
        $output["value"]["total"]++;
        $output["value"]["ratio"][0]++;
      }
      
      if ($scoreGuest === 20){
        $output["value"]["total"]++;
        $output["value"]["ratio"][1]++;
      }
    }
    
    return $output;
  }
  
  
  /**
   * @param $matches
   * @return array
   */
  public function matchesPerGameday($matches) {
    $flag = true;
    $output = array(
      "type" => "Bar",
      "css" => "widget-6col",
      "title" => "Begegnungen pro Wochentag",
      "label" => array('Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa', 'So'),
      "value" => array(0, 0, 0, 0, 0, 0, 0)
    );
  
    foreach ($matches as $match){
      if ($this->matchUtils->hasFixture($match)){
        $fixture = $match["fixture"];
        
        $output["value"][(date("N", strtotime($fixture))) - 1]++;
        $flag = false;
      }
    }
  
    if ($flag){
      $output = array();
    }
  
    return $output;
  }
}