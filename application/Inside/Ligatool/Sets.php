<?php

namespace Inside\Ligatool;

require_once(__DIR__ . '/MatchUtils.php');

use Inside\Ligatool\MatchUtils;

class Sets{
  
  private $matchUtils;
  
  /**
   * Number constructor.
   */
  public function __construct() {
    $this->matchUtils = new MatchUtils();
  }
  
  
  /**
   * @param $matches
   * @return array
   */
  public function total($matches) {
    $output = array(
      "type" => "Number",
      "css" => "widget-3col",
      "title" => "Sätze Gesamt",
      "value" => array(
        "total" => 0,
        "ratio" => array(0, 0)
      )
    );
  
    foreach ($matches as $match){
      if ($this->matchUtils->hasScore($match)){
        $scoreHome = $match["scoreHome"];
        $scoreGuest = $match["scoreAway"];
      
        $output["value"]["total"]++;
        $output["value"]["ratio"][0] += $scoreHome;
        $output["value"]["ratio"][1] += $scoreGuest;
      }
    }
  
    $output["value"]["total"] = $output["value"]["total"] * 20;
  
    return $output;
  }
}