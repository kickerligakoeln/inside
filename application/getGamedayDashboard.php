<?php

require_once(__DIR__ . '/Inside/KKL.php');
require_once(__DIR__ . '/Inside/Ligatool/MatchUtils.php');
require_once(__DIR__ . '/Inside/Ligatool/Games.php');
require_once(__DIR__ . '/Inside/Ligatool/Sets.php');
require_once(__DIR__ . '/Inside/Ligatool/Goals.php');
require_once(__DIR__ . '/Inside/Ligatool/Fixture.php');
require_once(__DIR__ . '/Inside/Spielbogen/Players.php');

use KKL\KKL;


$output = array();
if (isset($_GET["gameday"])){
  $KKL = new KKL();
  
  $matchUtils = new \Inside\Ligatool\MatchUtils();
  $games = new \Inside\Ligatool\Games();
  $sets = new \Inside\Ligatool\Sets();
  $goals = new \Inside\Ligatool\Goals();
  $fixture = new \Inside\Ligatool\Fixture();
  
  $players = new \Inside\Spielbogen\Players();;
  
  $unfilteredMatches = $KKL->getMatchesPerGameday($_GET["gameday"]);
  $allMatches = $matchUtils->getFilteredMatches($unfilteredMatches);
  
  $unfilteredScoresheets = $KKL->getScoresheetsPerGameday($_GET["gameday"]);
  
  if (count($allMatches) > 0){
    array_push($output, $unfilteredScoresheets);
  
    array_push($output, $games->total($allMatches));
    array_push($output, $sets->total($allMatches));
    array_push($output, $goals->total($allMatches));
    array_push($output, $games->failures($allMatches));
    
    array_push($output, $games->results($allMatches));
    array_push($output, $games->distribution($allMatches));
    array_push($output, $games->ratio($allMatches));
  
    array_push($output, $players->total($unfilteredScoresheets));
    array_push($output, $fixture->perWeekday($allMatches));
  }
  
  echo json_encode($output, true);
} else{
  echo json_encode(array(), true);
}
