<?php

require_once(__DIR__ . '/config/config.php');

/**
 * @param $season
 * @return array|mixed
 */
function getGamedays($season){
  $jsonGameday = file_get_contents(KKL_API_URL . "/seasons/" . $season . "/gamedays");
  
  if ($jsonGameday){
    $allGamedays = json_decode($jsonGameday, true);
    
    return $allGamedays;
  } else{
    return array();
  }
}

$season = NULL;
if(isset($_GET["season"])){
  $season = $_GET["season"];
}

$output = getGamedays($season);
echo json_encode($output, true);