<?php

require_once(__DIR__ . '/config/config.php');

/**
 * @param null $customYear
 * @return array|bool|false|string
 */
function getSeasons($customYear = NULL){
  $seasons = array();
  $jsonAllSeasons = file_get_contents(KKL_API_URL . "/seasons");
  
  if ($jsonAllSeasons){
    $allSeasons = json_decode($jsonAllSeasons, true);
    
    if (!$customYear){
      $customYear = date("Y");
    }
    
    foreach ($allSeasons as $season){
      $posTest = strpos($season["name"], "Test");
      $posYear = strpos($season["startDate"], $customYear);
      
      if ($posYear !== false && $posTest === false){
        array_push($seasons, $season);
      }
    }
    
    return $seasons;
  } else{
    return array();
  }
}

$year = NULL;
if(isset($_GET["year"])){
  $year = $_GET["year"];
}

$output = getSeasons($year);
echo json_encode($output, true);