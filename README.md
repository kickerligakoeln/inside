# Kölner Kickerliga - Insides

## Usage

### required
* Node JS v9.9.0

### init
```bash
    $ npm install
    $ grunt 
```

### develop

```bash
  $ grunt <command>

  Commands:

    default     compile all files to dist/
    dev         [default task], create source-maps, start watcher
    docs        [default task], create sassdoc, create typedoc 
```

The `dev` task includes some useful extensions:
* create `sourceMaps` (Javascript, SCSS)
* create `sassdoc`
* start `watcher`

### live

```bash
    $ npm run grunt
    or
    $ node_modules/grunt-cli/bin/grunt
```


## Stylesheet

### ITCSS
ITCSS stands for [Inverted Triangle CSS](https://www.xfive.co/blog/itcss-scalable-maintainable-css-architecture/) and it 
helps you to organize your project CSS files in such way that you can better deal with (not always easy-to-deal with) 
CSS specifics like global namespace, cascade and selectors specificity.


### sassdoc [![npm version](https://badge.fury.io/js/grunt-sassdoc.svg)](https://badge.fury.io/js/grunt-sassdoc)
We will use a [SASS Documentation](http://sassdoc.com/) to explain our `mixins`, `functions` and `placeholder`.
Find the documentation here: ```/dist/css/sassdoc/index.html```



## Fonts & Icons

### icomoon [![npm version](https://badge.fury.io/js/icomoon-build.svg)](https://badge.fury.io/js/icomoon-build)
We use [Icomoon](https://icomoon.io/)  to organize the icon-stack. 

#### create
* Create new Project
* Choose Icons from Libraries
* Save **Projectfile** (*.json) here: ```/src/fonts/kkl-inside.json```

The Filename and -path is defined in ```package.json``` (line 16).

#### update in project

```bash
  $ npm run icomoon
```

#### usage
Use the existing mixin `_getIcon([icon-variable])` to get the Icon in your stylesheet. All possible icon-variables are 
defined in `src/scss/_______settings/_settings.icomoon.scss`


## Javascript

### Typescript [![npm version](https://badge.fury.io/js/typescript.svg)](https://badge.fury.io/js/typescript)

[TypeScript](https://www.typescriptlang.org/) is a typed superset of JavaScript that compiles to plain JavaScript.

### Valiant Tailor (selfmade)

**Valiant Tailor** is a selfmade Typescript framework to handle data and templates (mostly asynchronous).
Just set a HTML-Tag to call a Typescript-Class wich should handle all your data stuff. 

Therefore you have to create some Files and Folders:
* a new TypeScript-File
* a new Template folder (same name as Class!)
* some new template files (*.html)

##### components & widgets
````html
<vt-component name="Tutorial"></vt-component>
````

````typescript
class TutorialComponent extends BaseComponent {

    public init(): Q.Promise<boolean> {
        const deferred = Q.defer<boolean>();
        let data = {
            text: '[Hier wird eine Component ausgegen.]'
        };

        this.getTemplate("view.html", data).then((t: JQuery) => {
            this.config.container.append(t);
            console.log('component', this.config.container);

            deferred.resolve(true);
        });

        return deferred.promise;
    }
}
````


##### PubSub

You can - like eventListener - send and recieve messages from component / widget to another.
Just **create** or **join** a channel and send simple json-data by a topicname into this channel-scope. 

````typescript
let channel = PubSub.join('#channel-name-identifier');

channel.on('channel-topic-identifier', (resp) => {
    console.log('You recieved a message from PubSub:', resp);
});

channel.send('channel-topic-identifier', {date: 12345});
````

There are a lot more features. Find here: `/src/scripts/pubsub/*`


## Templating

### Nunjucks [![npm version](https://badge.fury.io/js/nunjucks.svg)](https://badge.fury.io/js/nunjucks)

The templates in **Valiant Tailor** will be rendered with [Nunjucks](https://mozilla.github.io/nunjucks/templating.html).